#!/usr/bin/python

# Copyright 2001-2003 James A. Treacy
# This code may be distributed under the terms of the GPL

import sys, urllib, htmllib, httplib, ftplib, formatter, urlparse, socket;
import re, signal
from sgmllib import SGMLParser

TIMEOUT = 5

def handler(signum, frame):
	# print 'Signal handler called with signal', signum
	raise IOError, "timed out accessing site"

def do_page(page_url):
	print "Looking into", page_url
	try:
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		current = urllib.urlopen(page_url)
		signal.alarm(0)
		# print current.geturl()
		# print current.read()
	except (IOError, EOFError), arg:
		print "Error accessing page:", arg.args[0]
		return
	parse = htmllib.HTMLParser(formatter.NullFormatter())
	parse.feed(current.read())
	parse.close()
	#if debug:
	#	print parse.anchorlist
	for url in parse.anchorlist:
		parts = urlparse.urlparse(url)
		parts = parts[0], parts[1], parts[2], parts[3], parts[4], ''
		urlnew = urlparse.urlunparse(parts)
		# print "TESTING:", url, urlnew
		url = urlparse.urljoin(page_url, urlnew)
		# print url
		check_url(url)
		httpdone[url] = 1

def check_url(url):
	# check url exist
	parts = urlparse.urlparse(url)
	parts = parts[0], parts[1], parts[2], parts[3], parts[4], ''
	urlnew = urlparse.urlunparse(parts)
	if debug:
		if urlnew != url:
			print "DEBUG: ", url, "converted to", urlnew
	url = urlnew
	if urlseen.has_key(url):
		if re.search('\(200\)', urlseen[url]) or re.search('\(302\)', urlseen[url]):
			# print "  found a good url so not printing it"
			pass
		else:
			print "  " + url + urlseen[url]
		return
	else:
		msg = "If you see this, then msg didn't get set properly"
		if parts[0] == 'ftp':
			msg = check_ftp(url, parts)
		elif parts[0] == 'https':
			msg = " : can't check https for validity"
		elif parts[0] == 'mailto':
			msg = " : can't check mailto for validity"
		elif parts[0] == 'news':
			msg = " : can't check news for validity"
		elif parts[0] == 'irc':
			msg = " : can't check irc for validity"
		elif parts[0] == 'file':
			try:
				current = urllib.urlopen(url)
				httplist.append(url)
				msg = " : is ok"
			except IOError, arg:
				msg = " : Error: "+ arg.args[1]
		elif parts[0] == 'http':
			msg = check_http(url, parts)
		elif parts[0] == 'https':
			msg = check_https(url, parts)
		else:
			msg = " : unknown url type"
		if re.search('\(200\)', msg) or re.search('\(302\)', msg):
			# print "  found a good url so not printing it"
			pass
		else:
			print "  " + url + msg
		urlseen[url] = msg

def check_http(url, parts):
	# must do http connection using httplib so can parse the return codes
	# only if the url is good should it be added to httplist
	# print "entering check_http with url =", url
	try:
		# print "host =", parts[1]
		# print "file =", parts[2]
		msg = ""
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		h = httplib.HTTP(parts[1])
		h.putrequest('HEAD', url)
		h.putheader('Accept', '*/*')
		h.endheaders()
		errcode, errmsg, headers = h.getreply()
		signal.alarm(0)
		# print "    errcode =",errcode
		# print "    errmsg  =",errmsg
		# print "    headers =",headers
		if errcode == 200 or errcode == 302:
			msg = " : (" + str(errcode) + ") " + errmsg 
			add_url(url)
		elif errcode == 301:
			headers = str(headers)
			#print "    headers =", headers
			result = re.findall("Location:\s*(.*)\n", headers)
			if len(result):
				if (result[0]):
				msg = " : Error = (" + str(errcode) + ") " + errmsg + ". New URL: " + result[0]
			else:
				msg = " : Error = (" + str(errcode) + ") " + errmsg
		elif errcode == 400 or errcode == 403 or errcode == 404:
			signal.signal(signal.SIGALRM, handler)
			signal.alarm(TIMEOUT)
			h = httplib.HTTP(parts[1])
			h.putrequest('HEAD', parts[2])
			h.endheaders()
			errcode, errmsg, headers = h.getreply()
			signal.alarm(0)
			# print "    headers =", headers
			if errcode == 200 or errcode == 302:
				add_url(url)
				msg = " : (" + str(errcode) + ") " + errmsg 
			else:
				msg = " : Error = (" + str(errcode) + ") " + errmsg 
		else:
			msg = " : Error = (" + str(errcode) + ") " + errmsg 
	except IOError, arg:
		msg = " : IOError: " + str(arg.args[0])
	except socket.error, arg:
		msg = " : Error: " + str(arg.args)
	except ValueError:
		msg = " : Error: URL not valid "
	return msg

def check_https(url, parts):
	# must do http connection using httplib so can parse the return codes
	# only if the url is good should it be added to httplist
	# print "entering check_http with url =", url
	try:
		# print "host =", parts[1]
		# print "file =", parts[2]
		msg = ""
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		h = httplib.HTTPS(parts[1])
		h.putrequest('HEAD', url)
		h.putheader('Accept', '*/*')
		h.endheaders()
		errcode, errmsg, headers = h.getreply()
		signal.alarm(0)
		# print "    errcode =",errcode
		# print "    errmsg  =",errmsg
		# print "    headers =",headers
		if errcode == 200 or errcode == 302:
			msg = " : (" + str(errcode) + ") " + errmsg 
			add_url(url)
		elif errcode == 301:
			headers = str(headers)
			#print "    headers =", headers
			result = re.findall("Location:\s*(.*)\n", headers)
			if len(result):
				if (result[0]):
				msg = " : Error = (" + str(errcode) + ") " + errmsg + ". New URL: " + result[0]
			else:
				msg = " : Error = (" + str(errcode) + ") " + errmsg
		elif errcode == 400 or errcode == 403 or errcode == 404:
			signal.signal(signal.SIGALRM, handler)
			signal.alarm(TIMEOUT)
			h = httplib.HTTP(parts[1])
			h.putrequest('HEAD', parts[2])
			h.endheaders()
			errcode, errmsg, headers = h.getreply()
			signal.alarm(0)
			# print "    headers =", headers
			if errcode == 200 or errcode == 302:
				add_url(url)
				msg = " : (" + str(errcode) + ") " + errmsg 
			else:
				msg = " : Error = (" + str(errcode) + ") " + errmsg 
		else:
			msg = " : Error = (" + str(errcode) + ") " + errmsg 
	except IOError, arg:
		msg = " : IOError: " + str(arg.args[0])
	except socket.error, arg:
		msg = " : Error: " + str(arg.args)
	except ValueError:
		msg = " : Error: URL not valid "
	return msg

def ftp_file_exists(string):
	kluge[0] = 1

def check_ftp(url, parts):
	try:
		signal.signal(signal.SIGALRM, handler)
		signal.alarm(TIMEOUT)
		ftp = ftplib.FTP(parts[1])
		ftp.login()
		# listcmd = "LIST " + parts[2]
		# print "    listcmd =", listcmd
		#ftp.retrlines(listcmd)
		kluge[0] = 0
		ftp.dir(parts[2], ftp_file_exists)
		if kluge[0]:
			msg = " : is ok"
		else:
			msg = " : Error: file doesn't exist"
		ftp.quit()
		signal.alarm(0)
	except socket.error, arg:
		msg = " : Error: " + str(arg.args)
	except (IOError, ftplib.error_perm, ftplib.error_temp, EOFError), arg:
		msg = " : Error: " + str(arg)
	#retrlines (command[, callback])
	#dir (argument[, ...])
	#cwd (pathname)
	return msg

def add_url(url):
	for x in require:
		if not re.search(x, url):
			# print "    ", url, "is missing", x
			return
	for x in ignore:
		if re.search(x, url):
			# print "    ", url, "includes", x, "which is being ignored"
			return
	httplist.append(url)
		

# BEGIN MAIN PROGRAM

debug=0
kluge = []
kluge.append(0)

if (len(sys.argv) > 1):
	starturl = sys.argv[1]
else:
	print 'Error: first argument should be the starting URL'
	sys.exit()

httplist = []
httpdone = {}
urlseen = {}
ignore = ['/Packages', 'News/weekly/oldurl', '/Lists-Archives']
require = ['www.debian.org']
#print "Looking into", starturl
#check_url(starturl)
#sys.exit()
httplist.append(starturl)
if len(httplist):
	url = httplist.pop(0)
else:
	print "Error: no http urls"
	sys.exit()
while (1):
	do_page(url)
	httpdone[url] = 1
	if (len(httplist) > 0):
		url = httplist.pop(0)
	else:
		break
print "Program Finished Normally"
