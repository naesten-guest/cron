#!/bin/bash

umask 002
. `dirname $0`/../common.sh

# build installation-guide for stable + testing/unstable + oldstable

# extract release codenames from webwml repo
stable_codename=`grep "define-tag current_release_name" $webtopdir/webwml/english/template/debian/release_info.wml | cut -d">" -f2 | cut -d"<" -f1`
testing_codename=`grep "define-tag current_testing_name" $webtopdir/webwml/english/template/debian/release_info.wml | cut -d">" -f2 | cut -d"<" -f1`
oldstable_codename=`grep "define-tag current_oldstable_name" $webtopdir/webwml/english/template/debian/release_info.wml | cut -d">" -f2 | cut -d"<" -f1`

# =====================================================================================================================================
# part 1: build installation-guide for stable from source package

# create a logfile for the stable manual build, if it does not already exist:
[ -f $webtopdir/installmanual/$stable_codename.log ] || touch $webtopdir/installmanual/$stable_codename.log
# symlink the logfile to htdocs/build-logs, if link is not already existing:
# (so we get the build log exported to https://www-master.debian.org/build-logs/webwml/installmanual/)
[ -f $htdocsbuildlogs/$stable_codename.log ] || ln -s $webtopdir/installmanual/$stable_codename.log $htdocsbuildlogs/$stable_codename.log

[ -d $crondir/tmp-stable ] || mkdir -p $crondir/tmp-stable
cd $crondir/tmp-stable

# Initial creation of ig-stable-built.txt file (needed to detect new package versions)
[ -f $crondir/log/ig-stable-built.txt ] || echo "installation-guide-20000101" >$crondir/log/ig-stable-built.txt

ig_version_stable=`rmadison --arch=source --suite=stable installation-guide | awk '{print $3}' | tail -n 1`
ig_dsc_stable=installation-guide_$ig_version_stable.dsc
ig_dir_stable=installation-guide-$ig_version_stable

### If a new version of installation-guide for stable is available:
###   - Extract the new source package from $ftpdir/stable dir
###   - Clean tmp directory (temporary holds the source package for build)
###   - Clear the old target dir in $webtopdir/installmanual/
###   - Build the new manual
###   - If the build is successful (= when the example-preseed exists; gets generated at the end of the build process):
###       - Copy the built files to $webdir/releases dir
###         (from there they will be sync'ed to www mirrors)
###       - Save version-number of the just built manual to ig-stable-built.txt;
###         needed in later runs of this script to detect new package versions.
###         (Also, one can simply remove this file, to trigger a re-build of the manual, if needed.)
###       - Copy the built files to $webdir/releases/$stable_codename/
###         HINT: in special situations (like when an architecture or language gets dropped from the build),
###         old files/directories from that architecture or language may staying behind in
###         $webdir/releases/$stable_codename/. You will need to drop them by hand.

   if [ `echo $ig_dsc_stable` != `cat $crondir/log/ig-stable-built.txt` ]; then
	rm -fR $crondir/tmp-stable/*
	echo "extracting installation-guide source for stable ($stable_codename) ..." >> $crondir/log/lessoften.log
	dpkg-source -x "$ftpdir/stable/$ig_dsc_stable" >> $crondir/log/lessoften.log

	rm -fR $webtopdir/installmanual/$stable_codename/*
	echo "building installation-guide for stable ($stable_codename) ..." >> $crondir/log/lessoften.log
	cd $ig_dir_stable/build &&
	echo "Started building installation-guide for stable ($stable_codename) at: `date`" > $webtopdir/installmanual/$stable_codename.log
	manual_release=$stable_codename destination=$webtopdir/installmanual/$stable_codename/ ./buildweb.sh >> $webtopdir/installmanual/$stable_codename.log 2>&1

	if [ -f "$webtopdir/installmanual/$stable_codename/example-preseed.txt" ]; then # Was the built successful?
	echo "copying stable ($stable_codename) installmanual in place ..." >> $crondir/log/lessoften.log
	cp -a $webtopdir/installmanual/$stable_codename/* $webdir/releases/$stable_codename/
	echo $ig_dsc_stable >$crondir/log/ig-stable-built.txt
	else
		echo "FAILED to build the installmanual for stable ($stable_codename) ..." >> $crondir/log/lessoften.log
	fi

   else
	echo "no new installation-guide for stable ($stable_codename), skipping build" >> $crondir/log/lessoften.log
   fi



# =====================================================================================================================================
# part 2: build installation-guide for testing from (unstable) source package

# create a logfile for the testing manual build, if it does not already exist:
[ -f $webtopdir/installmanual/$testing_codename.log ] || touch $webtopdir/installmanual/$testing_codename.log
# symlink the logfile to htdocs/build-logs, if link is not already existing:
# (so we get the build log exported to https://www-master.debian.org/build-logs/webwml/installmanual/)
[ -f $htdocsbuildlogs/$testing_codename.log ] || ln -s $webtopdir/installmanual/$testing_codename.log $htdocsbuildlogs/$testing_codename.log

[ -d $crondir/tmp-testing ] || mkdir -p $crondir/tmp-testing
cd $crondir/tmp-testing

# Initial creation of ig-testing-built.txt file (needed to detect new package versions)
[ -f $crondir/log/ig-testing-built.txt ] || echo "installation-guide-20001111" >$crondir/log/ig-testing-built.txt

# !!!     For the upcoming release ('testing'), we use the installation-guide from UNSTABLE here !!!
ig_version_testing=`rmadison --arch=source --suite=unstable installation-guide | awk '{print $3}' | tail -n 1`
ig_dsc_testing=installation-guide_$ig_version_testing.dsc
ig_dir_testing=installation-guide-$ig_version_testing

### If a new version of installation-guide for testing is available:
###   - Extract the new source package from $ftpdir/sid dir
###   - Clean tmp directory (temporary holds the source package for build)
###   - Clear the old target dir in $webtopdir/installmanual/
###   - Build the new manual
###   - If the build is successful (= when the example-preseed exists; gets generated at the end of the build process):
###       - Copy the built files to $webdir/releases dir
###         (from there they will be sync'ed to www mirrors)
###       - Save version-number of the just built manual to ig-testing-built.txt;
###         needed in later runs of this script to detect new package versions.
###         (Also, one can simply remove this file, to trigger a re-build of the manual, if needed.)
###       - Copy the built files to $webdir/releases/$testing_codename/
###         HINT: in special situations (like when an architecture or language gets dropped from the build),
###         old files/directories from that architecture or language may staying behind in
###         $webdir/releases/$stable_codename/. You will need to drop them by hand.

   if [ `echo $ig_dsc_testing` != `cat $crondir/log/ig-testing-built.txt` ]; then
	rm -fR $crondir/tmp-testing/*
	echo "extracting installation-guide source for testing ($testing_codename) ..." >> $crondir/log/lessoften.log
	dpkg-source -x "$ftpdir/sid/$ig_dsc_testing" >> $crondir/log/lessoften.log

	rm -fR $webtopdir/installmanual/$testing_codename/*
	echo "building installation-guide for testing ($testing_codename) ..." >> $crondir/log/lessoften.log
	cd $ig_dir_testing/build &&
	echo "Started building installation-guide for testing ($testing_codename) at: `date`" > $webtopdir/installmanual/$testing_codename.log
	manual_release=$testing_codename destination=$webtopdir/installmanual/$testing_codename/ ./buildweb.sh >> $webtopdir/installmanual/$testing_codename.log 2>&1

	if [ -f "$webtopdir/installmanual/$testing_codename/example-preseed.txt" ]; then # Was the built successful?
	echo "copying testing ($testing_codename) installmanual in place ..." >> $crondir/log/lessoften.log
	cp -a $webtopdir/installmanual/$testing_codename/* $webdir/releases/$testing_codename/
	echo $ig_dsc_testing >$crondir/log/ig-testing-built.txt
	else
		echo "FAILED to build the installmanual for testing ($testing_codename) ..." >> $crondir/log/lessoften.log
	fi

   else
	echo "no new installation-guide for testing ($testing_codename), skipping build" >> $crondir/log/lessoften.log
   fi


# =====================================================================================================================================
# part 3: build installation-guide for oldstable from source package

# create a logfile for the oldstable manual build, if it does not already exist:
[ -f $webtopdir/installmanual/$oldstable_codename.log ] || touch $webtopdir/installmanual/$oldstable_codename.log
# symlink the logfile to htdocs/build-logs, if link is not already existing:
# (so we get the build log exported to https://www-master.debian.org/build-logs/webwml/installmanual/)
[ -f $htdocsbuildlogs/$oldstable_codename.log ] || ln -s $webtopdir/installmanual/$oldstable_codename.log $htdocsbuildlogs/$oldstable_codename.log

[ -d $crondir/tmp-oldstable ] || mkdir -p $crondir/tmp-oldstable
cd $crondir/tmp-oldstable

# Initial creation of ig-oldstable-built.txt file (needed to detect new package versions)
[ -f $crondir/log/ig-oldstable-built.txt ] || echo "installation-guide-20000101" >$crondir/log/ig-oldstable-built.txt

ig_version_oldstable=`rmadison --arch=source --suite=oldstable installation-guide | awk '{print $3}' | tail -n 1`
ig_dsc_oldstable=installation-guide_$ig_version_oldstable.dsc
ig_dir_oldstable=installation-guide-$ig_version_oldstable

### If a new version of installation-guide for oldstable is available:
###   - Extract the new source package from $ftpdir/oldstable dir
###   - Clean tmp directory (temporary holds the source package for build)
###   - Clear the old target dir in $webtopdir/installmanual/
###   - Build the new manual
###   - If the build is successful (= when the example-preseed exists; gets generated at the end of the build process):
###       - Copy the built files to $webdir/releases dir
###         (from there they will be sync'ed to www mirrors)
###       - Save version-number of the just built manual to ig-oldstable-built.txt;
###         needed in later runs of this script to detect new package versions.
###         (Also, one can simply remove this file, to trigger a re-build of the manual, if needed.)
###       - Copy the built files to $webdir/releases/$oldstable_codename/
###         HINT: in special situations (like when an architecture or language gets dropped from the build),
###         old files/directories from that architecture or language may staying behind in
###         $webdir/releases/$oldstable_codename/. You will need to drop them by hand.

   if [ `echo $ig_dsc_oldstable` != `cat $crondir/log/ig-oldstable-built.txt` ]; then
	rm -fR $crondir/tmp-oldstable/*
	echo "extracting installation-guide source for oldstable ($oldstable_codename) ..." >> $crondir/log/lessoften.log
	dpkg-source -x "$ftpdir/oldstable/$ig_dsc_oldstable" >> $crondir/log/lessoften.log

	rm -fR $webtopdir/installmanual/$oldstable_codename/*
	echo "building installation-guide for oldstable ($oldstable_codename) ..." >> $crondir/log/lessoften.log
	cd $ig_dir_oldstable/build &&
	echo "Started building installation-guide for oldstable ($oldstable_codename) at: `date`" > $webtopdir/installmanual/$oldstable_codename.log
	manual_release=$oldstable_codename destination=$webtopdir/installmanual/$oldstable_codename/ ./buildweb.sh >> $webtopdir/installmanual/$oldstable_codename.log 2>&1

	if [ -f "$webtopdir/installmanual/$oldstable_codename/example-preseed.txt" ]; then # Was the built successful?
	echo "copying oldstable ($oldstable_codename) installmanual in place ..." >> $crondir/log/lessoften.log
	cp -a $webtopdir/installmanual/$oldstable_codename/* $webdir/releases/$oldstable_codename/
	echo $ig_dsc_oldstable >$crondir/log/ig-oldstable-built.txt
	else
		echo "FAILED to build the installmanual for oldstable ($oldstable_codename) ..." >> $crondir/log/lessoften.log
	fi

   else
	echo "no new installation-guide for oldstable ($oldstable_codename), skipping build" >> $crondir/log/lessoften.log
   fi
