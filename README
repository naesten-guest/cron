/srv/www.debian.org/cron

This directory contains all the scripts used to update the Debian web site.
The updates are started from the crontab of user debwww.

The regular updates are controlled by the file `often', which sets up a
lockfile. Its primary job is simply to execute run-parts on the
parts/ directory. The data files can be found in the datafiles/
directory.

IMPORTANT: if you add a file to parts/, make sure it is before
999TriggerMirrors alphabetically. We want mirroring to be the
last thing done.

The update is started with this crontab, time is in UTC:

24 3,7,11,15,19,23 * * * /srv/www.debian.org/cron/often

The log files for the updates are found in the log/ directory:
  git_update.log - output from the git update
  wml_run.log    - output from the wml update
  stattrans.log  - output form the translation stats
  often.log      - everything else

Daily updates are also made, started from the file `lessoften', at 14:49
(local time), and logged to file `lessoften.log' in the log/ directory.

= Hint for committer

Before you commit, test your script as follows:
 $ sudo mkdir -p /srv/www.debian.org
 $ cd /srv/www.debian.org
 $ sudo chown <username>:<usergroup> .
 $ git clone git@salsa.debian.org:webmaster-team/cron.git
 $ cd ; mkdir -p path/to ; cd ~/path/to
 $ git clone git@salsa.debian.org:webmaster-team/cron.git
 $ cd cron
   ... hack source ...

Then you can test parts/* scripts from <username> account.
For example:
 $ cd ~/path/to/cron/parts
 $ ./1ftpfiles
  ...
 $ ./7doc
  ...
  check what happens under
    * /srv/www.debian.org/cron/ftpfiles
    * /srv/www.debian.org/webwml
    * /srv/www.debian.org/www

Please note cron/often and cron/lessoften requires you to be UID=debwww.
